package com.br.tdd;

import java.util.List;

public class Leilao {
    private List<Lance> lances;

    public Leilao(List<Lance> lances) {
        this.lances = lances;
    }

    public List<Lance> getLances() {
        return lances;
    }

    public void setLances(List<Lance> lances) {
        this.lances = lances;
    }

    public Lance adicionarNovoLance(Lance lance) {

        if (this.validarLance(lance)) {
            this.lances.add(lance);
            return lance;
        } else {
            throw new RuntimeException("Lance inválido, valor menor que o lance anterior");
        }
    }

    public boolean validarLance(Lance lance) {
        boolean retorno = false;
        int tamanhoLista = this.getLances().size();

        if (this.getLances().size() > 0) {
            if (lance.getValorDoLance() > this.getLances().get(tamanhoLista - 1).getValorDoLance()) {
                retorno = true;
            } else {
                retorno = false;
            }
        } else {
            retorno = true;
        }

        return retorno;
    }
}

package com.br.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.w3c.dom.html.HTMLAnchorElement;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class LeilaoTeste {
    private Lance lance;
    private Leilao leilao;

    @BeforeEach
    public void Inicializar() {
        this.leilao = new Leilao(new ArrayList<>());
        this.lance = new Lance(new Usuario(1, "Participante"), 100.00);
    }

    @Test
    public void testarIncluirNovoLance() {
        Lance lanceResultado = this.leilao.adicionarNovoLance(this.lance);
        Assertions.assertSame(this.lance, lanceResultado);
    }

    @Test
    public void testarAdicionarMaisDeUmLanceParticipanteDiferente() {

        List<Lance> listaLanceEnviado = new ArrayList<>();

        //Incluir varios Lances de uma só vez
        for (int i = 0; i < 5; i++) {
            listaLanceEnviado.add(this.leilao.adicionarNovoLance(new Lance(new Usuario(i + 1, "Participante " + (i + 1)), 100.00 + (i * 50.00))));
        }

        Assertions.assertTrue(listaLanceEnviado.containsAll(this.leilao.getLances()) && this.leilao.getLances().containsAll(listaLanceEnviado));
    }

    @Test
    public void testarAdicionarMaisDeUmLanceMesmoParticipante() {

        List<Lance> listaLanceEnviado = new ArrayList<>();

        //Incluir varios Lances de uma só vez
        for (int i = 0; i < 5; i++) {
            listaLanceEnviado.add(this.leilao.adicionarNovoLance(new Lance(new Usuario(1, "Participante"), 100.00 + (i * 50.00))));
        }

        Assertions.assertTrue(listaLanceEnviado.containsAll(this.leilao.getLances()) && this.leilao.getLances().containsAll(listaLanceEnviado));
    }

    @Test
    public void testarIncluirNovoLanceDeValorMenor() {

        List<Lance> listaLanceEnviado = new ArrayList<>();

        /* Aqui já adiciono lances validos na lista para testar inserção de lance de menor valor
         * enviado no Assertions.assertThrows
         */
        for (int i = 0; i < 3; i++) {
            listaLanceEnviado.add(this.leilao.adicionarNovoLance(new Lance(new Usuario(i + 1, "Participante " + (i + 1)), 100.00 + (i * 50.00))));
        }

        Assertions.assertThrows(RuntimeException.class, () -> this.leilao.adicionarNovoLance(this.lance));
    }


}
